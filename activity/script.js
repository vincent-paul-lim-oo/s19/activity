console.log(`S19 Activity`);

const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const myAddress = [`258 Washington Ave NW,`, `California`, `90011`];
const [first, second, third] = myAddress;

console.log(`I live at ${first} ${second} ${third}`);

const myAnimal = {
    type: `saltwater crocodile`,
    weight: `1075 kgs`,
    measurement: `20 ft 3 in`
}

function getMyAnimal({ type, weight, measurement }) {
    console.log(`Lolong was a ${ type}. He weighed at ${weight} with a measurement of  ${measurement}.`);
}

getMyAnimal(myAnimal);

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(number);
})

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog()

myDog.name = `Frankie`;
myDog.age = 5;
myDog.breed = `Miniature Dachschund`;

console.log(myDog);